@echo off

setlocal

if exist .keystore goto error_AlreadyExists

@echo on

keytool -genkey -keystore .keystore -alias Clustering -storepass keystore.password -keypass Clustering.password

@echo off

goto end

:error_AlreadyExists
echo Error: a .keystore file already exists!

:end

endlocal
