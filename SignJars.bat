@echo off

setlocal

if not exist colt.jar goto error_Nocolt

@echo on

jarsigner -keystore .keystore -storepass keystore.password -keypass Clustering.password colt.jar Clustering

@echo off

goto end

:error_Nocolt
echo Error: attempting to non-existent file colt.jar

goto end

:end

endlocal
