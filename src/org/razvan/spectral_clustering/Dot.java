package org.razvan.spectral_clustering;

/**
 * @author <A HREF="mailto:surdules@gmail.com">Razvan Surdulescu</A>, Copyright (C) 2003
 */
public class Dot {
    private int m_x, m_y;

    public Dot(int x, int y) {
        m_x = x;
        m_y = y;
    }

    public int getX() {
        return m_x;
    }

    public int getY() {
        return m_y;
    }

    public double getWeight(Dot other, double spatialRadius, double spatialDelta) {
        double deltaX = getX() - other.getX();
        double deltaY = getY() - other.getY();
        double distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        if (distance < spatialRadius) {
            return Math.exp(-1 * distance / spatialDelta);
        } else {
            return 0;
        }
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("x=").append(m_x).append(",y=").append(m_y);
        return result.toString();
    }
}
