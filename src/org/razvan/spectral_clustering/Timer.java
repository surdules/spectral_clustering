package org.razvan.spectral_clustering;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <A HREF="mailto:surdules@gmail.com">Razvan Surdulescu</A>, Copyright (C) 2003
 */
public class Timer {
    private List<String> m_columns = new ArrayList<>();
    private List<Long> m_times = new ArrayList<>();

    private boolean m_startFlag;
    private long m_startTime;

    public void start(String name) {
        if (m_startFlag) {
            throw new IllegalStateException("Already start()ed: you need to end() first!");
        }

        m_startFlag = true;

        m_columns.add(name);
        m_startTime = System.currentTimeMillis();
    }

    public void end() {
        if (!m_startFlag) {
            throw new IllegalStateException("Not yet start()ed: you need to start() first!");
        }

        m_startFlag = false;

        m_times.add(new Long(System.currentTimeMillis() - m_startTime));
    }

    public String toString() {
        return toString(true);
    }

    public String toString(boolean showColumns) {
        StringBuffer result = new StringBuffer();

        String nl = System.getProperty("line.separator");

        if (showColumns) {
            for (int i = 0; i < m_times.size(); i++) {
                result.append("\"").append(m_columns.get(i)).append("\"").append(",");
            }
            result.append(nl);
        }

        for (int i = 0; i < m_times.size(); i++) {
            result.append(m_times.get(i)).append(",");
        }

        return result.toString();
    }
}
