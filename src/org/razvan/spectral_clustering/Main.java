package org.razvan.spectral_clustering;

/**
 * @author <A HREF="mailto:surdules@gmail.com">Razvan Surdulescu</A>, Copyright (C) 2003
 */
public class Main {
    public static void main(String[] args) {
        DotFrame frame = new DotFrame("Clustering Demo");
        frame.setSize(600, 600);
        frame.setVisible(true);
    }
}
